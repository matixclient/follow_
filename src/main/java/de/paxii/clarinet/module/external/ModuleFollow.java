package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.player.UpdatePlayerMoveStateEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.chat.Chat;
import de.paxii.clarinet.util.module.killaura.EntityManager;
import de.paxii.clarinet.util.player.PlayerUtils;

import net.minecraft.entity.player.EntityPlayer;

/**
 * Created by Lars on 27.08.2016.
 */
public class ModuleFollow extends Module {
  private String target = "";
  private EntityPlayer targetPlayer;
  private EntityManager entityManager;

  public ModuleFollow() {
    super("Follow", ModuleCategory.MOVEMENT);

    this.setBuildVersion(17500);
    this.setVersion("1.1");
    this.setRegistered(true);
    this.setCommand(true);
    this.setSyntax("follow <player>");
    this.setDescription("Automatically follows a player. The name is case-sensitive.");

    this.entityManager = new EntityManager(null, Wrapper.getFriendManager());
  }

  @EventHandler
  public void onUpdateMoveState(UpdatePlayerMoveStateEvent moveStateEvent) {
    if (target.length() > 0 && this.targetPlayer == null) {
      this.targetPlayer = Wrapper.getWorld().getPlayerEntityByName(this.target);
    }

    if (this.targetPlayer != null) {
      if (Wrapper.getPlayer().getDistanceToEntity(this.targetPlayer) > 100.0F || this.targetPlayer.isDead) {
        this.targetPlayer = null;
        return;
      }

      float[] angles = this.entityManager.getAngles(targetPlayer);

      Wrapper.getPlayer().rotationYaw = angles[0];
      Wrapper.getPlayer().rotationPitch = angles[1];
      Wrapper.getPlayer().rotationYawHead = angles[0];

      if (Wrapper.getModuleManager().isModuleActive("Fly")) {
        if (this.targetPlayer.getPosition().getY() > Wrapper.getPlayer().getPosition().getY()) {
          Wrapper.getPlayer().motionY += 1;
        } else if (this.targetPlayer.getPosition().getY() < Wrapper.getPlayer().getPosition().getY()) {
          Wrapper.getPlayer().motionY -= 1;
        }
      }

      if (Wrapper.getPlayer().isCollidedHorizontally && Wrapper.getPlayer().onGround) {
        Wrapper.getPlayer().jump();
      }

      moveStateEvent.setMoveForward(1.0F);
    }
  }

  @Override
  public void onCommand(String[] args) {
    if (args.length > 0) {
      this.target = args[0];
      this.targetPlayer = PlayerUtils.getPlayerByName(this.target);

      if (this.targetPlayer == null) {
        Chat.printClientMessage("The player you entered could not be found nearby. I will continue searching...");
      } else {
        Chat.printClientMessage(String.format("Now following %s.", this.targetPlayer.getName()));

        if (!this.isEnabled()) {
          this.setEnabled(true);
        }
      }
    } else {
      Chat.printClientMessage("Too few arguments!");
    }
  }
}
